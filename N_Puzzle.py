from collections import deque

class Nodo:
    def __init__(self, estado_actual, nodo_padre, nodo_mov, nodo_profundidad, piezas_correctas):        
        self.estado_actual = estado_actual                       
        self.nodo_padre = nodo_padre                          
        self.nodo_mov = nodo_mov                
        self.nodo_profundidad = nodo_profundidad              
        self.piezas_correctas = piezas_correctas 
    
    def movimiento(self, direccion):
        estado_actual = list(self.estado_actual)
        ind = estado_actual.index(0)

        if direccion == "arriba":            
            if ind not in [6, 7, 8]:                
                temp = estado_actual[ind + 3]
                estado_actual[ind + 3] = estado_actual[ind]
                estado_actual[ind] = temp
                return tuple(estado_actual)
            else:                
                return None

        elif direccion == "abajo":            
            if ind not in [0, 1, 2]:                
                temp = estado_actual[ind - 3]
                estado_actual[ind - 3] = estado_actual[ind]
                estado_actual[ind] = temp
                return tuple(estado_actual)
            else:                
                return None

        elif direccion == "derecha":            
            if ind not in [0, 3, 6]:                
                temp = estado_actual[ind - 1]
                estado_actual[ind - 1] = estado_actual[ind]
                estado_actual[ind] = temp
                return tuple(estado_actual)
            else:                
                return None

        elif direccion == "izquierda":            
            if ind not in [2, 5, 8]:                
                temp = estado_actual[ind + 1]
                estado_actual[ind + 1] = estado_actual[ind]
                estado_actual[ind] = temp
                return tuple(estado_actual)
            else:                
                return None           


    def encontrar_sucesores(self):
        sucesores = []
        sucesorN = self.movimiento("arriba")
        sucesorS = self.movimiento("abajo")
        sucesorE = self.movimiento("derecha")
        sucesorO = self.movimiento("izquierda")
        
        sucesores.append(Nodo(sucesorN, self, "arriba", self.nodo_profundidad + 1, calcular_heurisitica(sucesorN)))
        sucesores.append(Nodo(sucesorS, self, "abajo", self.nodo_profundidad + 1, calcular_heurisitica(sucesorS)))
        sucesores.append(Nodo(sucesorE, self, "derecha", self.nodo_profundidad + 1, calcular_heurisitica(sucesorE)))
        sucesores.append(Nodo(sucesorO, self, "izquierda", self.nodo_profundidad + 1, calcular_heurisitica(sucesorO)))
        
        sucesores = [nodo for nodo in sucesores if nodo.estado_actual != None]  
        return sucesores   


    def encontrar_camino(self, inicial):
        camino = []
        nodo_actual = self
        while nodo_actual.nodo_profundidad >= 1:
            camino.append(nodo_actual)
            nodo_actual = nodo_actual.nodo_padre
        camino.reverse()
        return camino

    def imprimir_nodo(self):
        renglon = 0
        for pieza in self.estado_actual:
            if pieza == 0:
                print(" ", end = " ")
            else:
                print (pieza, end = " ")
            renglon += 1
            if renglon == 3:
                print()
                renglon = 0       


def calcular_heurisitica(estado):
    correcto = (1, 2, 3, 4, 5, 6, 7, 8, 0)
    valor_correcto = 0
    piezas_correctas = 0
    if estado:
        for valor_pieza, valor_correcto in zip(estado, correcto):
            if valor_pieza == valor_correcto:
                piezas_correctas += 1
            valor_correcto += 1
    return piezas_correctas   

def bfs(inicial, meta):
    visitados = set()
    frontera = deque()    
    frontera.append(Nodo(inicial, None, None, 0, calcular_heurisitica(inicial)))
    
    while frontera:                         
        nodo = frontera.popleft()           

        if nodo.estado_actual not in visitados:    
                visitados.add(nodo.estado_actual)      
        else:                               
            continue                        
        
        if nodo.estado_actual == meta:                          
            print("\n¡Se encontró la meta!")            
            return nodo.encontrar_camino(inicial)              
        else:                                           
            frontera.extend(nodo.encontrar_sucesores())        


def dfs(inicial, meta, profundidad_max):
    visitados = set()  
    frontera = deque()  
    frontera.append(Nodo(inicial, None, None, 0, calcular_heurisitica(inicial)))
    
    while frontera:                         
        nodo = frontera.pop()               

        if nodo.estado_actual not in visitados:     
            visitados.add(nodo.estado_actual)      
        else:                               
            continue                        
        
        if nodo.estado_actual == meta:             
            print("\n¡Se encontró la meta!")            
            return nodo.encontrar_camino(inicial)
        else:                                            
            if profundidad_max > 0:                             
                if nodo.nodo_profundidad < profundidad_max:                         
                    frontera.extend(nodo.encontrar_sucesores()) 
            else:                                               
                frontera.extend(nodo.encontrar_sucesores())         
   

def main():
    estado_final = (1, 2, 3, 4, 5, 6, 7, 8, 0)
    estado_inicial = (8, 7, 5, 3, 0, 1, 4, 2, 6)

    #Menú principal
    print("Este programa encuentra la solución al 8-puzzle\nutilizando diferentes algoritmos.")
    print("El estado inicial del juego es: ")
    (Nodo(estado_inicial, None, None, 0, calcular_heurisitica(estado_inicial))).imprimir_nodo()
    print("\n¿Qué algoritmo desea correr? Escriba:")
    print("\t\"bfs\" para  correr Busqueda de anchura")
    print("\t\"dfs\" para  correr Budqueda en profundidad")
    print("\tCualquier otra cosa para terminar el programa.")
    algoritmo = input("Su elección: ")


    if algoritmo == "bfs" or algoritmo == "BFS":
        print("Corriendo BFS. Por favor espere.")
        nodos_camino = bfs(estado_inicial, estado_final)

    elif algoritmo == "dfs" or algoritmo == "DFS":
        print("\n¿Establecer un límite de profundidad?")
        print("Escriba el límite como un entero mayor que 0")
        print("o cualquier otro entero para continuar sin límite.")
        profundidad_max = int(input("Profundidad: "))
        print("Corriendo DFS. Por favor espere.")
        nodos_camino = dfs(estado_inicial, estado_final, profundidad_max)
    
    else:
        return 0   

 
    if nodos_camino:
        print ("El camino tiene", len(nodos_camino), "movimientos.")
        imprimir_camino = (input ("¿Desea imprimir dicho camino? s/n: "))

        if imprimir_camino == "s" or imprimir_camino == "S":
            print("\nEstado inicial:")
            (Nodo(estado_inicial, None, None, 0, calcular_heurisitica(estado_inicial))).imprimir_nodo()
            print ("Piezas correctas:", calcular_heurisitica(estado_inicial), "\n")
            input("Presione \"enter\" para continuar.")
            
            for nodo in nodos_camino:
                print("\nSiguiente movimiento:", nodo.movimiento)
                print("Estado actual:")
                nodo.imprimir_nodo()
                print("Piezas correctas:", nodo.piezas_correctas, "\n")     
                input("Presione \"enter\" para continuar.")
    else:
        print ("\nNo se encontró un camino con las condiciones dadas.")

    return 0            

if __name__ == "__main__":
    main()       
raw_input()
    

    

